#!/bin/sh

set -e

make --directory=conf
make --directory=tp-turtle
make --directory=tp-game-of-life

zip -r site/tp-game-of-life.zip tp-game-of-life/src

cp conf/conf.pdf site/conf.pdf
cp tp-turtle/tp.pdf site/tp-turtle.pdf
cp tp-game-of-life/tp.pdf site/tp-game-of-life.pdf

make --directory=conf clean
make --directory=tp-turtle clean
make --directory=tp-game-of-life clean
