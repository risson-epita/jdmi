import tkinter, functools
import random as rand

def init_plateau(hauteur, largeur, element):
    plateau = []
    for i in range(hauteur):
        ligne = []
        for i in range(largeur):
            ligne.append(element)
        plateau.append(ligne)
    return plateau

def malea(plateau):
    for i in range(len(plateau)):
        for j in range(len(plateau[i])):
            plateau[i][j] = rand.randint(0, 1)
    return plateau

def regle(etat_cellule, voisins_vivants):
    if etat_cellule == 0 and voisins_vivants == 3:
        return 1
    if etat_cellule == 1 and (voisins_vivants < 2 or 3 < voisins_vivants):
        return 0
    return etat_cellule

def voisins_vivants(plateau, x, y):
    if x == 0 and y == 0:
        return plateau[0][1] + plateau[1][0] + plateau[1][1]
    if x == 0 and y == len(plateau[0]) - 1:
        return plateau[0][y - 1] + plateau[1][y - 1] + plateau[1][y]
    if x == len(plateau) - 1 and y == 0:
        return plateau[x - 1][0] + plateau[x - 1][1] + plateau[x][1]
    if x == len(plateau) - 1 and y == len(plateau[0]) - 1:
        return plateau[x - 1][y] + plateau[x][y - 1] + plateau[x - 1][y - 1]
    if x == 0:
        return plateau[0][y - 1] + plateau[0][y + 1] + \
               plateau[1][y - 1] + plateau[1][y] + plateau[1][y + 1]
    if y == 0:
        return plateau[x - 1][0] + plateau[x + 1][0] + \
               plateau[x - 1][1] + plateau[x][1] + plateau[x + 1][1]
    if x == len(plateau) - 1:
        return plateau[x][y - 1] + plateau[x][y + 1] + \
               plateau[x - 1][y - 1] + plateau[x - 1][y] + plateau[x - 1][y + 1]
    if y == len(plateau[0]) - 1:
        return plateau[x - 1][y] + plateau[x + 1][y] + \
               plateau[x - 1][y - 1] + plateau[x][y - 1] + plateau[x + 1][y - 1]
    return plateau[x - 1][y - 1] + plateau[x - 1][y] + plateau[x - 1][y + 1] + \
           plateau[x][y - 1] + plateau[x][y + 1] + \
           plateau[x + 1][y - 1] + plateau[x + 1][y] + plateau[x + 1][y + 1]

def prochain_tour(plateau):
    nouveau_plateau = init_plateau(len(plateau), len(plateau[0]), 0)
    for i in range(len(plateau)):
        for j in range(len(plateau[0])):
            nouveau_plateau[i][j] = regle(plateau[i][j], voisins_vivants(plateau, i, j))
    return nouveau_plateau

def afficher_plateau(can, plateau):
    can.delete("all")
    for i in range(len(plateau)):
        for j in range(len(plateau[0])):
            if (plateau[i][j]):
                can.create_rectangle(i * 10, j * 10, i * 10 + 10, j * 10 + 10, fill="black")

def start_game(fen, can):
    plateau = init_plateau(60, 80, 0)
    plateau = malea(plateau)
    def loop(plateau):
        plateau = prochain_tour(plateau)
        afficher_plateau(can, plateau)
        fen.after(50, lambda: loop(plateau))
    loop(plateau)

fen = tkinter.Tk()
can = tkinter.Canvas(width=800, height=600)
can.pack()

start_game(fen, can)

fen.mainloop()
