import tkinter
import random as rand

HAUTEUR = 80
LARGEUR = 80
CELL_SIZE = 10
CELL_COLOR = "black"

def init_plateau(hauteur, largeur, element):
    """
    Créer une matrice de taille hauteur x largeur
    et dont toutes les cases contiennent element
    """
    pass

def malea(plateau):
    """
    Remplir le plateau de valeurs entières aléatoires entre 0 et 1 compri
    """
    pass

def regle(etat_cellule, voisins_vivants):
    """
    Appliquer la règle du jeu de la vie sur la cellule d'état etat_cellule
    en fonction du nombre de voisins vivants.
    """
    pass

def voisins_vivants(plateau, x, y):
    """
    Calculer le nombre de voisins vivants de la cellule de coordonnées x, y
    """
    pass

def prochain_tour(plateau):
    """
    Appliquer la règle du jeu de la vie sur toutes les cellules et renvoier
    le plateau
    """
    pass

def afficher_plateau(canvas, plateau):
    """ Fonction d'aide à l'affichage du plateau """
    canvas.delete("all")
    for i in range(len(plateau)):
        for j in range(len(plateau[0])):
            if (plateau[i][j]):
                canvas.create_rectangle(i * CELL_SIZE,
                                        j * CELL_SIZE,
                                        i * CELL_SIZE + CELL_SIZE,
                                        j * CELL_SIZE + CELL_SIZE,
                                        fill=CELL_COLOR)

def start_game(fenetre, canvas):
    """
    Démarrer le jeu !
    Premièrement, initialiser le plateau
    Ensuite, calculer le plateau du prochain tour et l'afficher,
    ceci en boucle
    """
    # Initialiser le plateau et le remplir de valeurs aléatoires
    # écrivez ici
    # Pour cela, utilisez les variables HAUTEUR et LARGEUR
    # écrivez ici
    def loop(plateau):
        # Calculer le plateau du prochain tour
        # écrivez ici
        # Afficher le plateau
        # écrivez ici
        fenetre.after(50, lambda: loop(plateau))
    loop(plateau)

def main():
    """
    Point d'entrée de notre programme
    """
    fenetre = tkinter.Tk()
    canvas = tkinter.Canvas(width=LARGEUR * CELL_SIZE, height=HAUTEUR * CELL_SIZE)
    canvas.pack()
    start_game(fenetre, canvas)
    fenetre.mainloop()


########
# main()
########
# Ecrivez vos tests ci-dessous
# et lorsque vous avez fini, décommentez la ligne ci-dessus pour lancer le Jeu
########
